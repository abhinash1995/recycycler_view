package com.example.recyclerviewcardview;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemDetailsActivity extends AppCompatActivity {
    private TextView pName;
    private TextView pCaption;
    private ImageView pImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_details);
        pName = findViewById(R.id.name);
        pCaption = findViewById(R.id.caption);
        pImg = findViewById(R.id.imageView);
//// Recieve data
        Intent intent = getIntent();
        String caption = intent.getExtras().getString("Caption");
        String name = intent.getExtras().getString("Name");
        int image = intent.getExtras().getInt("Image");
//setting the data
        pName.setText(name);
        pCaption.setText(caption);
        pImg.setImageResource(image);

    }

}
