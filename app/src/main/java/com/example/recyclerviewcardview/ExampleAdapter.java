package com.example.recyclerviewcardview;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;


public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {
    private List<ExampleItem> mExampleList;
    private Context mContext ;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder {
        //cretaing view holder

        private ImageView mImageView;
        private TextView mTextView1;
        private TextView mTextView2;
        private CardView cardView;

        private ExampleViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView);
            mTextView1 = itemView.findViewById(R.id.textView);
            mTextView2 = itemView.findViewById(R.id.textView2);
            cardView=itemView.findViewById(R.id.cardId);
        }
    }


    //constructor when we create object of example adapter we have to pass array list or item
    public ExampleAdapter(Context mContext, List<ExampleItem> mExampleList) {
        this.mContext = mContext;
        this.mExampleList = mExampleList;
    }

    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //what we want to assign to view
        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.example_item,parent,false);
        return new ExampleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ExampleViewHolder holder, final int position) {
        ExampleItem currentItem = mExampleList.get(position);

        holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mTextView1.setText(currentItem.getmText1());
        holder.mTextView2.setText(currentItem.getmText2());

        //to open next activity on recycler view
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,ItemDetailsActivity.class);
                intent.putExtra("Name",mExampleList.get(position).getmText1());
                intent.putExtra("Caption",mExampleList.get(position).getmText2());
                intent.putExtra("Image",mExampleList.get(position).getmImageResource());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}