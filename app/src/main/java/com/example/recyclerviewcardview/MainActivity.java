package com.example.recyclerviewcardview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       List<ExampleItem> exampleList = new ArrayList<>();
        exampleList.add(new ExampleItem(R.drawable.jaleshwor_mahadev, "Jaleshwor", "Known for Jaleshwor Nath Mandir"));
        exampleList.add(new ExampleItem(R.drawable.janaki_mandir_janakpur, "Janakpur", "Known For Janaki Mandir"));
        exampleList.add(new ExampleItem(R.drawable.pashupatinath, "Kathmandu", "Known for Pashupatinath Mandir"));
        exampleList.add(new ExampleItem(R.drawable.manakamana, "Gorkha", "Known For Manakamana devi"));
        exampleList.add(new ExampleItem(R.drawable.muktinath_temple_jumla, "Jumla", "Known for Muktinath Temple"));
        exampleList.add(new ExampleItem(R.drawable.sashwatdham_chitwan, "Chitwan", "Known for Sashwatdham"));
        exampleList.add(new ExampleItem(R.drawable.baijnath, "Jharkhand", "Known for baba baijnath dham"));
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(this,exampleList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}